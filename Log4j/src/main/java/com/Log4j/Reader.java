package com.Log4j;

public interface Reader {
    public String read();
}
