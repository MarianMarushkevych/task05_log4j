package com.Log4j;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    public static final String ACCOUNT_SID = "ACeea738b2b162c9429bab6a1b00234e23";
    public static final String AUTH_TOKEN = "2395a9ce4f9911bdbd5c6621f2d3e77c";

    public static void send(String s){
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380930539599"), // to
                        new PhoneNumber("+14694052732"), // from
                        s).create();
    }

}
